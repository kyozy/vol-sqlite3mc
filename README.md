# vol-sqlite3mc

#### 介绍
目前基于SQLite 3.45.1 版本 

火山模块，封装自 SQLite3MultipleCiphers 
[https://github.com/utelle/SQLite3MultipleCiphers](https://github.com/utelle/SQLite3MultipleCiphers)

主要目的是解决SQLite3加密数据库的问题.


#### 安装配置

1. 直接安装 zySqlite3mc.vcip 即可使用 (在发行版中下载)

#### 注意事项

1. 主要的类为 `数据库SQLite3mc` 和 `记录集SQLite3mc`, 其他是一些辅助类
2. 数据库 和 记录集 类都必须手动关闭
3. main.v 里面有简单使用例子
